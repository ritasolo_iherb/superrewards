// TIMER //

// samples -- will eventually connect to spreadsheet with existing Rewards end dates
const rewardOne = new Date("2019-03-01T03:00:00+00:00").getTime();
const rewardTwo = new Date("2019-02-01T03:00:00+00:00").getTime();

// display function takes in the end date const of reward and a string of the ID of the div where it will be displayed
const displayTimer = (reward, timer) =>
  setInterval(function () {
    const now = new Date().getTime();
    const distance = reward - now;

    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    function twoDigits(n) {
      return (n < 10 ? "0" : "") + n;
    }

    // display timer
    document.getElementById(timer).innerHTML = `<p>${twoDigits(days)}d
        ${twoDigits(hours)}:${twoDigits(minutes)}:${twoDigits(seconds)}</p>`;

    // if less than day, turn red
    if (distance <= 86400000) {
      document.getElementById(timer).setAttribute("style", "color:red;");
    }

    // if timer reaches zero, display "promo ended"
    if (distance < 0) {
      clearInterval(timer);
      document.getElementById(timer).innerHTML = `<p>Promo Ended</p>`;
      document
        .getElementById(timer)
        .setAttribute("style", "background:#E76953;");
    }
  });

displayTimer(rewardOne, "timer");
displayTimer(rewardTwo, "timer2");

// show / hide share

const shareModal = document.getElementById("shareModal");

// Show Share Modal
document.addEventListener('click', function (event) {
  if (event.target.classList.contains('shareButton')) {
    // Do something...
    shareModal.style.display = 'block';
  }
}, false);

// Hide Share Modal
document.addEventListener('click', function (event) {
  if (event.target.classList.contains('closeButtonLink')) {
    shareModal.style.display = 'none';
  }
}, false);